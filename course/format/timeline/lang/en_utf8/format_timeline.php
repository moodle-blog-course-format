<?php
$string['formattimeline'] = 'Timeline format'; 
$string['nametimeline'] = 'Timeline'; 
$string['newsection'] = 'New Section';
$string['showall'] = 'Show All Sections';
$string['showrecent'] = 'Show Most Recent Sections';
$string['headerinstructions'] = 'Use the first button above to add a new section to the top of the course format. If you want to move sections around use the show all button above.';
$string['sectionhint'] = 'Click here to edit this section\'s topic. ';
$string['datehint'] = 'Use a label and place it at the top to specify the date.';
?>
