Timeline Course Format
======================
This course format was designed so that teachers could add 1 section to the course at a time. The course can potentially have a very big number of sections. Only the first 10 sections are displayed by default. The user can use pagination to see the rest of the sections. 

Installation
--------------
To install this course format unzip the timeline.zip folder in the course/formats directory of your mooodle installation.

Patches
--------------
The patches/* files can be applied to your moodle installation. They fix a couple of bugs that Moodle had with the timeline course. 
* The course-edit patch makes sure that the course/edit page shows the current number of sections in the number of sections dropdown.
* The ajax patch takes care of fixing the drag and drop sorting of sections for the timeline course.
