<?php // $Id$
      // Display the whole course as "topics" made of of modules
      // In fact, this is very similar to the "weeks" format, in that
      // each "topic" is actually a week.  The main difference is that
      // the dates aren't printed - it's just an aesthetic thing for
      // courses that aren't so rigidly defined by time.
      // Included from "view.php"
      
    require_once($CFG->libdir.'/ajax/ajaxlib.php');
    require_once($CFG->dirroot.'/course/format/timeline/lib.php');

    // Add a new section to the bottom of the course if the user clicked on the new section button
    timeline_process_add_section($course);
  
    $topic = optional_param('topic', -1, PARAM_INT);
    $page = optional_param('page', 1, PARAM_INT);
    $sectionsperpage = optional_param('sectionsperpage', 10, PARAM_INT);
    $showall = optional_param('showall', -1, PARAM_INT);

    if ($showall === 1) {
        if (isset($_POST['showall'])) {
            @header($CFG->wwwroot.'/course/view.php?id='.$course->id.'&showall=1');
        }
        $sectionsperpage = $course->numsections;
    } else {
        if ($showall == 0) {
            @header($CFG->wwwroot.'/course/view.php?id='.$course->id);
        }
    }

    // Bounds for block widths
    // more flexible for theme designers taken from theme config.php
    $lmin = (empty($THEME->block_l_min_width)) ? 100 : $THEME->block_l_min_width;
    $lmax = (empty($THEME->block_l_max_width)) ? 210 : $THEME->block_l_max_width;
    $rmin = (empty($THEME->block_r_min_width)) ? 100 : $THEME->block_r_min_width;
    $rmax = (empty($THEME->block_r_max_width)) ? 210 : $THEME->block_r_max_width;

    define('BLOCK_L_MIN_WIDTH', $lmin);
    define('BLOCK_L_MAX_WIDTH', $lmax);
    define('BLOCK_R_MIN_WIDTH', $rmin);
    define('BLOCK_R_MAX_WIDTH', $rmax);

    $preferred_width_left  = bounded_number(BLOCK_L_MIN_WIDTH, blocks_preferred_width($pageblocks[BLOCK_POS_LEFT]),  
                                            BLOCK_L_MAX_WIDTH);
    $preferred_width_right = bounded_number(BLOCK_R_MIN_WIDTH, blocks_preferred_width($pageblocks[BLOCK_POS_RIGHT]), 
                                            BLOCK_R_MAX_WIDTH);

    if ($topic != -1) {
        $displaysection = course_set_display($course->id, $topic);
    } elseif ($page == 1 && !isediting($course->id) && $showall != 1) {
        $displaysection = timeline_latest_section($course, $sections);
    } else {
        $displaysection = course_set_display($course->id, 0);
    }

    $context = get_context_instance(CONTEXT_COURSE, $course->id);

    if (($marker >=0) && has_capability('moodle/course:setcurrentsection', $context) && confirm_sesskey()) {
        $course->marker = $marker;
        if (! set_field("course", "marker", $marker, "id", $course->id)) {
            error("Could not mark that topic for this course");
        }
    }
    $editing          = $PAGE->user_is_editing();

/// Layout the whole page as three big columns.
    echo '<table id="layout-table" cellspacing="0" summary="'.get_string('layouttable').'"><tr>';

    /// The left column ...
    $lt = (empty($THEME->layouttable)) ? array('left', 'middle', 'right') : $THEME->layouttable;
    foreach ($lt as $column) {
        switch ($column) {
            case 'left':
                timeline_print_left_col($pageblocks, $editing, $preferred_width_right, $page);
                break;
            case 'middle':
                /// Start main column
                timeline_print_main_col($course, $sections, $context, $editing, $showall, $page, $sectionsperpage, $displaysection);
                break;
            case 'right':
                // The right column
                timeline_print_right_col($pageblocks, $editing, $preferred_width_right);
                break;
        }
    }
    echo '</tr></table>';

