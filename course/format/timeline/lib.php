<?php 
/**
 * Print the pagination links at the bottom of the course page. The pagination links are shown 
 * if there is more than 1 page. 
 *
 * @param object $course
 * @param int $page             The page number that we are looking at
 * @param int $sectionsperpage  The sections to display per page
 * @param int $showall          Whether or not the teacher is viewing all the sections
 */
function timeline_print_pagination_links($course, $page, $sectionsperpage, $showall) {
    $showrecenttext = get_string('showrecent', 'format_timeline');
    $total_pages = ceil($course->numsections / $sectionsperpage);
    if ($total_pages > 1) {
        echo 'Page: ';
    }
    if ($page > 1) { // display Previous Link
        echo ' (<a href="view.php?id='.$course->id.'&amp;topic=0&amp;page='.($page-1).'">Previous</a>) ';
    }
    if ($total_pages > 1) {
        for ($pagenum = 1; $pagenum <= $total_pages; $pagenum++) {
            if ($pagenum == $page) {
                echo " $pagenum";
            } else {
                echo ' <a href="view.php?id='.$course->id.'&amp;topic=0&amp;page='.$pagenum.'">'.$pagenum.'</a>';
            }
        }
        if ($course->numsections > ($page * $sectionsperpage))  { // display Next link
            echo ' (<a href="view.php?id='.$course->id.'&amp;page='.($page+1).'">Next</a>)';
        }
    }
}

/**
 * Displays the form so that teachers can add one more section to the course.
 * This form is only displayed if the person is allowed to update the course
 * and it is in editing mode
 *
 * @param object $course        The course that the user is viewing
 * @param int $showall          Whether or not we are displaying all the sections
 */
function timeline_print_header_form($course, $showall) {
    $canupdate = has_capability('moodle/course:update', get_context_instance(CONTEXT_COURSE, $course->id));
    if (isediting($course->id) && $canupdate) {
        echo '<div id="addsection">';
        $newsectiontext = get_string('newsection', 'format_timeline');
        $showalltext = get_string('showall', 'format_timeline');
        $showrecenttext = get_string('showrecent', 'format_timeline');
        print_single_button('view.php?id='.$course->id, array('newsection'=>1), $newsectiontext, 'post');

        if ($showall === 1) {
            print_single_button('view.php?id='.$course->id.'&showall=0', null, $showrecenttext, 'post');
        } else {
            print_single_button('view.php?id='.$course->id.'&showall=1', null, $showalltext, 'post');
        }
        echo '</div>';
        echo '<p><span class="help-hints">'.get_string('headerinstructions', 'format_timeline').'</span></p>';
    }
}

/**
 * Adds one more section to the bottom of a course. It also updates the
 * modinfo cache.
 *
 * @param object $course        The course we are adding a section to
 */
function timeline_process_add_section(&$course) {
    global $CFG;
    $canupdate = has_capability('moodle/course:update', get_context_instance(CONTEXT_COURSE, $course->id));
    // Ceck to make sure that user has permission
    if (isediting($course->id) && $canupdate) {
        $newsection = optional_param('newsection', -1, PARAM_INT);
        if ($newsection !== -1) {
            $course->numsections++;
            $updatedcourse = set_field('course', 'numsections', $course->numsections, 'id', $course->id);
            if ($updatedcourse === false) {
                print_error('coursenotupdated');
            } else {
                // we now rebuild the cache so that modinfo gets updated
                rebuild_course_cache($course->id);

                // Trigger events
                events_trigger('course_updated', $course);

                redirect($CFG->wwwroot.'/course/view.php?id='.$course->id, 'New Section Added Successfully');
            }
        }
    }
}

/**
 * Figures out the latest section in a course. This is the default section displayed
 * to users when they first view a course.
 *
 * If this is a brand new course with no visible sections, we display the first 
 * section. 
 *
 * @param object $course
 * @param array $sections
 * @return int                  The latest most recent section.
 */
function timeline_latest_section($course, $sections) {
    for ($section_index = $course->numsections; $section_index > 0; $section_index--) {
        if (!empty($sections[$section_index]) && $sections[$section_index]->visible) {
            return $section_index;
        }
    }

    return 1;
}

/**
 * This method can be used to test whether or not a course is new. This method will
 * only return true the first time a course has been loaded after it was created.
 *
 * @param object $course
 * @param array $sections
 * @return boolean 
 */
function timeline_is_new_course($course, $sections) {
    for ($i = 1; $i < $course->numsections; $i++) {
        if (!empty($sections[$i])) {
            return false; // we found at least onesection
        }
    }
    return true;
}

function timeline_print_left_col($pageblocks, $editing, $preferred_width_left) {
    global $PAGE;
    if (blocks_have_content($pageblocks, BLOCK_POS_LEFT) || $editing) {
        echo '<td style="width:'.$preferred_width_left.'px" id="left-column">';
        print_container_start();
        blocks_print_group($PAGE, $pageblocks, BLOCK_POS_LEFT);
        print_container_end();
        echo '</td>';
    }
}

/**
 * This function takes care of printing the right column of the course format.
 *
 * @param array $pageblocks
 * @param bool $editing
 * @param preferred_width_right 
 */
function timeline_print_right_col($pageblocks, $editing, $preferred_width_right) {
    global $PAGE;
    if (blocks_have_content($pageblocks, BLOCK_POS_RIGHT) || $editing) {
        echo '<td style="width:'.$preferred_width_right.'px" id="right-column">';
        print_container_start();
        blocks_print_group($PAGE, $pageblocks, BLOCK_POS_RIGHT);
        print_container_end();
        echo '</td>';
    }
}

/**
 * Prints the main column that lists the section in a course
 */
function timeline_print_main_col($course, $sections, $context, $editing, $showall, $page, $sectionsperpage, $displaysection) {
    global $CFG, $mods, $modnames, $modnamesused, $USER;
    $streditsummary   = get_string('editsummary');
    $strtopichint     = get_string('sectionhint', 'format_timeline');
    $strdatehint      = get_string('datehint', 'format_timeline');
    $stradd           = get_string('add');
    $stractivities    = get_string('activities');
    $strshowalltopics = get_string('showalltopics');
    $strtopic         = get_string('topic');
    $strgroups        = get_string('groups');
    $strgroupmy       = get_string('groupmy');

    if ($editing) {
        $strstudents = moodle_strtolower($course->students);
        $strtopichide = get_string('topichide', '', $strstudents);
        $strtopicshow = get_string('topicshow', '', $strstudents);
        $strmarkthistopic = get_string('markthistopic');
        $strmarkedthistopic = get_string('markedthistopic');
        $strmoveup = get_string('moveup');
        $strmovedown = get_string('movedown');
    }

    echo '<td id="middle-column">';
    print_container_start();
    echo skip_main_destination();

    print_heading_block(get_string('topicoutline'), 'outline');
    timeline_print_header_form($course, $showall);

    echo '<table class="timeline" width="100%" summary="'.get_string('layouttable').'">';

/// If currently moving a file then show the current clipboard
    if (ismoving($course->id)) {
        $stractivityclipboard = strip_tags(get_string('activityclipboard', '', addslashes($USER->activitycopyname)));
        $strcancel= get_string('cancel');
        echo '<tr class="clipboard">';
        echo '<td colspan="3">';
        echo $stractivityclipboard.'&nbsp;&nbsp;(<a href="mod.php?cancelcopy=true&amp;sesskey='.$USER->sesskey.'">'.$strcancel.'</a>)';
        echo '</td>';
        echo '</tr>';
    }

/// Print Section 0

    $section = 0;
    $thissection = $sections[$section];
    $newcourse = timeline_is_new_course($course, $sections);

    if ($thissection->summary or $thissection->sequence or isediting($course->id)) {
        echo '<tr id="section-0" class="section main">';
        echo '<td class="left side">&nbsp;</td>';
        echo '<td class="content">';
        
        echo '<div class="summary">';
        $summaryformatoptions->noclean = true;
        echo format_text($thissection->summary, FORMAT_HTML, $summaryformatoptions);

        if (isediting($course->id) && has_capability('moodle/course:update', get_context_instance(CONTEXT_COURSE, $course->id))) {
            echo '<a title="'.$streditsummary.'" '.
                 ' href="editsection.php?id='.$thissection->id.'"><img src="'.$CFG->pixpath.'/t/edit.gif" '.
                 ' alt="'.$streditsummary.'" /></a><br /><br />';
        }
        echo '</div>';

        print_section($course, $thissection, $mods, $modnamesused);

        if (isediting($course->id)) {
            print_section_add_menus($course, $section, $modnames);
        }

        echo '</td>';
        echo '<td class="right side">&nbsp;</td>';
        echo '</tr>';
        echo '<tr class="section separator"><td colspan="3" class="spacer"></td></tr>';
    }


/// Now all the normal modules by topic
/// Everything below uses "section" terminology - each "section" is a topic.

    $timenow = time();
    $sectionmenu = array();
    $firstsectionpage = $course->numsections - $sectionsperpage * ($page - 1);
    $lastsectionpage = $firstsectionpage - $sectionsperpage;
    if ($lastsectionpage < 0) {
        $lastsectionpage = 0;
    }

    // If the user has selected one section to view, we set the sections per page
    // so that the for loop iterates only over one section
    if ($displaysection != 0) {
        $firstsectionpage = $displaysection;
        $lastsectionpage = $displaysection - 1;
    }

    for ($section = $firstsectionpage; $section > $lastsectionpage; $section--) {
        if (!empty($sections[$section])) {
            $thissection = $sections[$section];

        } else {
            unset($thissection);
            $thissection->course = $course->id;   // Create a new section structure
            $thissection->section = $section;
            $thissection->summary = '';
            if ($section == 1 && $newcourse) {
                $thissection->visible = 1;
            } else {
                $thissection->visible = 0;
            }
            if (!$thissection->id = insert_record('course_sections', $thissection)) {
                notify('Error inserting new topic!');
            }
        }

        $showsection = (has_capability('moodle/course:viewhiddensections', $context) or $thissection->visible or !$course->hiddensections);

        if (!empty($displaysection) and $displaysection != $section) {
            if ($showsection) {
                $strsummary = strip_tags(format_string($thissection->summary,true));
                if (strlen($strsummary) < 57) {
                    $strsummary = ' - '.$strsummary;
                } else {
                    $strsummary = ' - '.substr($strsummary, 0, 60).'...';
                }
                $sectionmenu['topic='.$section] = s($section.$strsummary);
            }
            continue;
        }

        if ($showsection) {

            $currenttopic = ($course->marker == $section);

            $currenttext = '';
            if (!$thissection->visible) {
                $sectionstyle = ' hidden';
            } else if ($currenttopic) {
                $sectionstyle = ' current';
                $currenttext = get_accesshide(get_string('currenttopic','access'));
            } else {
                $sectionstyle = '';
            }

            if (!has_capability('moodle/course:viewhiddensections', $context) and !$thissection->visible) {   
                // Hidden for students
            } else {
                echo '<tr id="section-'.$section.'" class="section main'.$sectionstyle.'">';
                echo '<td class="left side sectionnum">'.$currenttext.$section.'</td>';

                echo '<td class="content">';
                echo '<div class="summary">';
                $summaryformatoptions->noclean = true;
                echo format_text($thissection->summary, FORMAT_HTML, $summaryformatoptions);

                if (isediting($course->id) && has_capability('moodle/course:update', get_context_instance(CONTEXT_COURSE, $course->id))) {
                    echo ' <a title="'.$streditsummary.'" href="editsection.php?id='.$thissection->id.'">';
                    if ($thissection->summary == '') {
                        echo '<span class="help-hints">'.$strtopichint.'</span>';
                    }
                    echo '<img src="'.$CFG->pixpath.'/t/edit.gif" alt="'.$streditsummary.'" /></a><br />';
                    // show/hide date label hint
                    $modinfo = get_fast_modinfo($course);
                    $showlblhint = true;
                    if (!empty($thissection->sequence)) {
                        $sectionmods = explode(",", $thissection->sequence);
                        foreach ($sectionmods as $modnumber) {
                            $mod = $mods[$modnumber];
                            if ($mod->modname == "label") {
                                $showlblhint = false;
                            }
                        }
                    }
                    if ($showlblhint) {
                        echo '<span class="help-hints">'.$strdatehint.'</span>';
                    }
                }
                echo '</div>';

                print_section($course, $thissection, $mods, $modnamesused);

                if (isediting($course->id)) {
                    print_section_add_menus($course, $section, $modnames);
                }
            echo '</td>';

            echo '<td class="right side">';
            if ($displaysection == $section) {      // Show the zoom boxes
                echo '<a href="view.php?id='.$course->id.'&amp;topic=0#section-'.$section.'" title="'.$strshowalltopics.'">'.
                     '<img src="'.$CFG->pixpath.'/i/all.gif" alt="'.$strshowalltopics.'" /></a><br />';
            } else {
                $strshowonlytopic = get_string('showonlytopic', '', $section);
                echo '<a href="view.php?id='.$course->id.'&amp;topic='.$section.'" title="'.$strshowonlytopic.'">'.
                     '<img src="'.$CFG->pixpath.'/i/one.gif" alt="'.$strshowonlytopic.'" /></a><br />';
            }

            if (isediting($course->id) && has_capability('moodle/course:update', get_context_instance(CONTEXT_COURSE, $course->id))) {
                if ($course->marker == $section) {  // Show the "light globe" on/off
                    echo '<a href="view.php?id='.$course->id.'&amp;marker=0&amp;sesskey='.$USER->sesskey.'#section-'.$section.'" title="'.$strmarkedthistopic.'">'.
                         '<img src="'.$CFG->pixpath.'/i/marked.gif" alt="'.$strmarkedthistopic.'" /></a><br />';
                } else {
                    echo '<a href="view.php?id='.$course->id.'&amp;marker='.$section.'&amp;sesskey='.$USER->sesskey.'#section-'.$section.'" title="'.$strmarkthistopic.'">'.
                         '<img src="'.$CFG->pixpath.'/i/marker.gif" alt="'.$strmarkthistopic.'" /></a><br />';
                }

                if ($thissection->visible) {        // Show the hide/show eye
                    echo '<a href="view.php?id='.$course->id.'&amp;hide='.$section.'&amp;sesskey='.$USER->sesskey.'#section-'.$section.'" title="'.$strtopichide.'">'.
                         '<img src="'.$CFG->pixpath.'/i/hide.gif" alt="'.$strtopichide.'" /></a><br />';
                } else {
                    echo '<a href="view.php?id='.$course->id.'&amp;show='.$section.'&amp;sesskey='.$USER->sesskey.'#section-'.$section.'" title="'.$strtopicshow.'">'.
                         '<img src="'.$CFG->pixpath.'/i/show.gif" alt="'.$strtopicshow.'" /></a><br />';
                }

                if ($section < $course->numsections && $showall !== -1) {    // Add a arrow to move section up
                    echo '<a href="view.php?id='.$course->id.'&amp;random='.rand(1,10000).'&amp;section='.$section.'&amp;move=1&amp;sesskey='.$USER->sesskey.'#section-'.($section+1).'" title="'.$strmoveup.'">'.
                         '<img src="'.$CFG->pixpath.'/t/up.gif" alt="'.$strmoveup.'" /></a><br />';
                }

                if ($section > 1 && $showall !== -1) {                       // Add a arrow to move section down
                    echo '<a href="view.php?id='.$course->id.'&amp;random='.rand(1,10000).'&amp;section='.$section.'&amp;move=-1&amp;sesskey='.$USER->sesskey.'#section-'.($section-1).'" title="'.$strmovedown.'">'.
                         '<img src="'.$CFG->pixpath.'/t/down.gif" alt="'.$strmovedown.'" /></a><br />';
                }

            }

            echo '</td></tr>';
            echo '<tr class="section separator"><td colspan="3" class="spacer"></td></tr>';
            }
        }

    }
    echo '</table>';

    if (!empty($sectionmenu)) {
        echo '<div class="jumpmenu">';
        echo popup_form($CFG->wwwroot.'/course/view.php?id='.$course->id.'&amp;', $sectionmenu,
                   'sectionmenu', '', get_string('jumpto'), '', '', true);
        echo '</div>';
    }

    if ($displaysection == 0) {
        timeline_print_pagination_links($course, $page, $sectionsperpage, $showall);
    }
    print_container_end();
    echo '</td>';

}

?>
