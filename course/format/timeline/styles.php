span.help-hints {
    font-style:italic;
}
#addsection {
    padding-left: 40%;
    padding-bottom:0.8em;
}
#addsection .singlebutton input {
    height: 3em;
    min-width: 10em;
    margin-right:2em;
}
#addsection .singlebutton {
    min-width: 13em;
    display:inline;
}
.sectionnum {
    color:#777;
}
